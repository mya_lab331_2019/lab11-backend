package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.StudentAnotherDao;
import se331.lab.rest.dao.StudentDao;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentAnotherServiceImpl implements StudentAnotherService{
    @Autowired
    StudentAnotherDao studentAnotherDao;
    @Override
    public List<Student> getStudentByNameContains(String partOfName) {
        List<Student> students = studentAnotherDao.getAllStudent();
        List<Student> output = new ArrayList<>();
        for (Student student: students) {
            if(student.getName().toLowerCase().contains(partOfName.toLowerCase())){
                    output.add(student);
            }
        }
        return output;
        //return studentAnotherDao.getStudentByNameContains(partOfName);
    }

    @Override
    public List<Student> getStudentWhoseAdvisorNameIs(String advisorName) {
        List<Student> students = studentAnotherDao.getAllStudent();
        List<Student> output = new ArrayList<>();
        for (Student student: students) {
            String name = student.getAdvisor().getName();
            if (name.equalsIgnoreCase(advisorName)){
                output.add(student);
            }
        }
        return output;
        //return studentAnotherDao.getStudentWhoseAdvisorNameIs(advisorName);
    }
}
