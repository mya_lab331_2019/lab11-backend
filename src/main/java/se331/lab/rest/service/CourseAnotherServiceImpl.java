package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseAnotherDao;
import se331.lab.rest.entity.Course;


import java.util.ArrayList;
import java.util.List;

@Service
public class CourseAnotherServiceImpl implements CourseAnotherService {
    @Autowired
    CourseAnotherDao courseAnotherDao;
    @Override
    public List<Course> getCourseWhichStudentEnrolledMoreThan(int value) {
        List<Course> courses = courseAnotherDao.getAllCourse();
        List<Course> output = new ArrayList<>();
        for (Course course: courses) {
            int courseAmount = course.getStudents().size();
            if (courseAmount >= value){
                output.add(course);
            }
        }
        return output;
    }
}
